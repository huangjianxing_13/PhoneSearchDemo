package com.example.phonesearchdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.phonesearchdemo.bean.VResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {


    Button btsend;
    TextView tvshow1;
    TextView tvshow2;
    TextView tvshow3;
    EditText edphone;
    EditText edtb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvshow1 = findViewById(R.id.textView_a);
        tvshow2 = findViewById(R.id.textView_b);
        tvshow3 = findViewById(R.id.textView_c);
      //  edphone = findViewById(R.id.edt_a);
        edtb = findViewById(R.id.edt_b);
        btsend = findViewById(R.id.bt_send);

        btsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendPhone();

            }});}

            private void setTextView(final String city, final String company, final String province) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        tvshow1.setText("省份：" + province); //启动线程更新ui
                        tvshow2.setText("城市：" + city); //启动线程更新ui
                        tvshow3.setText("运营商：" + company); //启动线程更新ui

                    }
                });
            }

            private void sendPhone() {

                 // 网络耗时操作，需要使用线程

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String phoneString = edtb.getText().toString();
                        String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
                     if (Pattern.matches(regex,phoneString)) {
                     OkHttpClient okHttpClient = new OkHttpClient();
                        //构建请求头
                        Request request = new Request.Builder().url("https://apis.juhe" +
                                ".cn/mobile/get?phone=" + phoneString + "&dtype=&key" +
                                "=a3eaf52975c81e6156b4d28abe3e638c").get().build();

                          //设置请求
                        Call call = okHttpClient.newCall(request);
                        try {

                            Response response = call.execute();//获取请求后返回的值
                             // 解析json，将json转成实例化的bean类
                            Gson gson = new Gson();

                            VResponse vResponse = gson.fromJson(response.body().string(), VResponse.class);
                          Log.e("bean", vResponse.getResult().getCity());

                            setTextView(vResponse.getResult().getCity(), vResponse.getResult().getCompany(), vResponse.getResult().getProvince());
                        } catch (IOException e) {
                            e.printStackTrace();}}
                       else {
                           String province = "输入错误";;
                           String city ="输入错误" ;
                            String company ="输入错误" ;
                          setTextView(city,company,province);

                        }
                    }
                }).start();;
            }}