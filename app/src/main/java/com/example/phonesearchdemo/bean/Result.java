package com.example.phonesearchdemo.bean;

public class Result {


    private String province;//省份

    private  String city;//城市

    private  String company; //	运营商

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }




}
