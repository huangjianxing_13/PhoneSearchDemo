package com.example.phonesearchdemo.bean;

public class VResponse {
    private String resultcode;//返回码
    private String reason;//返回说明
    private Result result;//返回结果集
    private String erron_code; //错误返回码

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getResultcode() {
        return resultcode;
    }

    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getErron_code() {
        return erron_code;
    }

    public void setErron_code(String erron_code) {
        this.erron_code = erron_code;
    }
}
